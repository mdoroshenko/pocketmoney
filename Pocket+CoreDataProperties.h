//
//  Pocket+CoreDataProperties.h
//  PocketMoney
//
//  Created by Максим Дорошенко on 09.03.16.
//  Copyright © 2016 Max Doroshenko. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "Pocket.h"

NS_ASSUME_NONNULL_BEGIN

@interface Pocket (CoreDataProperties)

@property (nullable, nonatomic, retain) NSNumber *absoluteDLimit;
@property (nullable, nonatomic, retain) NSNumber *curDayLimit;
@property (nullable, nonatomic, retain) NSNumber *curDayNumber;
@property (nullable, nonatomic, retain) NSNumber *dailySpent;
@property (nullable, nonatomic, retain) NSNumber *daysInMonth;
@property (nullable, nonatomic, retain) NSNumber *monthlyLimit;
@property (nullable, nonatomic, retain) NSNumber *monthlySpent;
@property (nullable, nonatomic, retain) NSNumber *startDayLimit;
@property (nullable, nonatomic, retain) NSNumber *curMonthNumber;

@end

NS_ASSUME_NONNULL_END
