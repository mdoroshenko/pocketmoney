//
//  DailySpents+CoreDataProperties.m
//  PocketMoney
//
//  Created by Максим Дорошенко on 28.02.16.
//  Copyright © 2016 Max Doroshenko. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "DailySpents+CoreDataProperties.h"

@implementation DailySpents (CoreDataProperties)

@dynamic number;
@dynamic spent;

@end
