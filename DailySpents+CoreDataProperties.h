//
//  DailySpents+CoreDataProperties.h
//  PocketMoney
//
//  Created by Максим Дорошенко on 28.02.16.
//  Copyright © 2016 Max Doroshenko. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "DailySpents.h"

NS_ASSUME_NONNULL_BEGIN

@interface DailySpents (CoreDataProperties)

@property (nullable, nonatomic, retain) NSNumber *number;
@property (nullable, nonatomic, retain) NSNumber *spent;

@end

NS_ASSUME_NONNULL_END
