//
//  KeyboardButton.h
//  PocketMoney
//
//  Created by Максим Дорошенко on 01.03.16.
//  Copyright © 2016 Max Doroshenko. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface KeyboardButton : UIButton

@property (nonatomic, strong, nonnull) UIColor *keyColor;
@property (nonatomic, strong, nonnull) UIColor *pressedKeyColor;
@property (nonatomic, strong, nonnull) UIColor *keyTitleColor;

@property (nonatomic, strong, nonnull) UIColor *currentColor;
@property (nonatomic, strong, nonnull) UIColor *shadowColor;

@end
