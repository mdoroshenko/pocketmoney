//
//  ColorsPalette.h
//  PocketMoney
//
//  Created by Максим Дорошенко on 04.03.16.
//  Copyright © 2016 Max Doroshenko. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface ColorsPalette : NSObject

@property (nonatomic, strong, nonnull) UIColor *backgroundColor;
@property (nonatomic, strong, nonnull) UIColor *backgroundColorWithAlpha;
@property (nonatomic, strong, nonnull) UIColor *keyColor;
@property (nonatomic, strong, nonnull) UIColor *pressedKeyColor;
@property (nonatomic, strong, nonnull) UIColor *keyTitleColor;
@property (nonatomic, strong, nonnull) UIColor *lightColor;
@property (nonatomic, strong, nonnull) UIColor *darkColor;

// Graph colors
@property (nonatomic, strong, nonnull) UIColor *graphLineColor;
@property (nonatomic, strong, nonnull) UIColor *graphCircleColor;
// Graph colors for low rates
@property (nonatomic, strong, nonnull) UIColor *lowGraphLineColor;
@property (nonatomic, strong, nonnull) UIColor *lowGraphCircleColor;

@property (nonatomic, strong, nonnull) UIColor *shadowColor;

@property (nonatomic, strong, nonnull) UIColor *titlesColor;

+ (_Nonnull instancetype)sharedInstance;

@end
