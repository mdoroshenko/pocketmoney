//
//  SpentView.h
//  PocketMoney
//
//  Created by Максим Дорошенко on 26.02.16.
//  Copyright © 2016 Max Doroshenko. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BigView.h"
#import "SpentLabel.h"
#import "KeyboardDelegate.h"

@interface KeyboardView : BigView

// Label for print values
@property (strong, nonatomic, nullable) UILabel *label;

// BOOL for cancel button, or fullsize OK button
@property (nonatomic, assign) BOOL canBeCancelled;

// Animated present this view in <parentView> view if isVisible = NO
- (void)presentInView:(UIView  * _Nonnull)parentView endAnimationPoint:(CGPoint)point;

// Delegate
@property (nonatomic, weak, nullable) id<KeyboardDelegate> delegate;

@end
