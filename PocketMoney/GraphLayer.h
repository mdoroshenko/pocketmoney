//
//  GraphLayer.h
//  PocketMoney
//
//  Created by Максим Дорошенко on 03.03.16.
//  Copyright © 2016 Max Doroshenko. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>
#import "GraphPoint.h"

@interface GraphLayer : CALayer

@property (nonatomic, strong, nonnull) UIColor *mainColor;

@property (nonatomic, nonnull) CGPathRef path;

@property (nonatomic, assign) CGFloat strokeWidth;
@end
