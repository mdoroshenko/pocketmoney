//
//  NewMonthViewController.h
//  PocketMoney
//
//  Created by Максим Дорошенко on 26.02.16.
//  Copyright © 2016 Max Doroshenko. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UIView+Keyboard.h"

extern NSString *const NewMonthViewController_storyboardIdentifier;

@interface NewMonthViewController : UIViewController <UITextFieldDelegate, KeyboardDelegate, UITextViewDelegate>

@end
