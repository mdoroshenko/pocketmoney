//
//  UITextField+Utils.h
//  PocketMoney
//
//  Created by Максим Дорошенко on 07.03.16.
//  Copyright © 2016 Max Doroshenko. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UITextField (Utils)

// Fit font to height
- (void)fitToHeight;

@end
