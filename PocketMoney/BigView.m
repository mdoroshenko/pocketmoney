//
//  BigView.m
//  PocketMoney
//
//  Created by Максим Дорошенко on 01.03.16.
//  Copyright © 2016 Max Doroshenko. All rights reserved.
//

#import "BigView.h"
#import "InterfaceConstants.h"
#import "UIView+Helpers.h"

@implementation BigView

- (void)drawRect:(CGRect)rect {
    self.backgroundColor = [UIColor clearColor];
    [super drawRect:rect];
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextSaveGState(context);
    // Create shadow
    CGContextSetShadow(context, CGSizeMake(pockmon_icSHADOW_OFFSET_X, pockmon_icSHADOW_OFFSET_Y), pockmon_icSHADOW_BLUR);
    CGRect insetRect = CGRectInset(rect, pockmon_icBIGVIEW_INSET, pockmon_icBIGVIEW_INSET);
    [self drawRaundedRect:context inRect:insetRect roundedRadius:pockmon_icCORNER_RADIUS];
    CGContextRestoreGState(context);
}

@end
