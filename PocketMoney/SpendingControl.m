//
//  SpendingControl.m
//  PocketMoney
//
//  Created by Максим Дорошенко on 25.02.16.
//  Copyright © 2016 Max Doroshenko. All rights reserved.
//

#import "SpendingControl.h"
#import "CoreData/CoreData.h"
#import "AppDelegate.h"
#import "NotificationConstants.h"
#import "NSCalendar+PocketMoneyUtils.h"

@implementation SpendingControl

- (instancetype)init
{
    self = [super init];
    if (self) {
        [self initData];
    }
    return self;
}

+ (id)sharedInstance {
    static dispatch_once_t onceToken;
    static SpendingControl *sharedInstance;
    dispatch_once(&onceToken, ^{
        sharedInstance = [[SpendingControl alloc] init];
    });
    return sharedInstance;
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

// Initiate Data, detects if new month starts
- (void)initData {
    DLog(@"...raised");
    [self checkForNewDay];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(checkForNewDay) name:NSCalendarDayChangedNotification object:nil];
}

// Checks if curDay != lastSavedDay
// Selector for NSCalendarDayChangedNotification
- (void)checkForNewDay {
    NSUInteger newDay = [[NSCalendar currentCalendar] getCurrentDayNumber];
    NSUInteger sCurDNumber = [GlobalVariables sharedInstance].curDayNumber;
    if (sCurDNumber == 0 || newDay < sCurDNumber || [GlobalVariables sharedInstance].curMonthNumber != [[NSCalendar currentCalendar] getCurrentMonth]) {
        [self newMonthStarts];
    } else if (sCurDNumber != newDay) {
        [GlobalVariables sharedInstance].curDayNumber = newDay;
    }
}

// Coins spent
// Must recalculate dailySpent, save to CoreData
- (void)spentCoins:(NSUInteger)num {
    DLog(@"...raised");
    [GlobalVariables sharedInstance].dSpent += num;
    [GlobalVariables sharedInstance].mSpent += num;
}

// New week starts, calculate monthlySpent, clear weeklySpent
- (void)newWeekStarts {
    DLog(@"...raised");
}

- (void)newMonthStarts {
    DLog(@"...raised");
    [[NSNotificationCenter defaultCenter] postNotificationName:NNAME_newMonthStarts object:self];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(startNewMonth:) name:NNAME_userActivatesNewMonthWithLimit object:nil];
}

// New month starts, clear all data, save new monthlyLimit
- (void)startNewMonth:(NSNotification *)notification {
    DLog(@"...raised");
    [[NSNotificationCenter defaultCenter] removeObserver:self name:NNAME_userActivatesNewMonthWithLimit object:nil];
    NSDictionary *userInfo = [notification userInfo];
    NSUInteger newMonthlyLimit = [[userInfo valueForKey:NKEY_newLimit] integerValue];
    [GlobalVariables sharedInstance].mLim = newMonthlyLimit;
}

// MARK: KeyboardDelegate
- (void)inputEnds:(NSUInteger)inputValue {
    [self spentCoins:inputValue];
}

@end
