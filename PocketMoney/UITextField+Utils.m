//
//  UITextField+Utils.m
//  PocketMoney
//
//  Created by Максим Дорошенко on 07.03.16.
//  Copyright © 2016 Max Doroshenko. All rights reserved.
//

#import "UITextField+Utils.h"
#import "InterfaceConstants.h"

@implementation UITextField (Utils)

// Fit font to height
- (void)fitToHeight {
    CGFloat maxFontSize = pockmon_icFONT_SIZE_MAX;
    CGFloat minFontSize = pockmon_icFONT_SIZE_MIN;
    CGFloat fontSizeAverage = 0.0f;
    CGFloat textAndLabelHeightDiff = 0.0f;
    CGFloat textAndLabelWidthDiff = 0.0f;
    
    while (minFontSize < maxFontSize) {
        fontSizeAverage = minFontSize + (maxFontSize - minFontSize) / 2.0f;
        
        if (self.text == nil) {
            break;
        } else {
            CGFloat labelHeight = self.bounds.size.height;
            CGFloat labelWidth = self.bounds.size.width;
            CGSize textStringSize = [self.text sizeWithAttributes:@{NSFontAttributeName: [UIFont systemFontOfSize:fontSizeAverage]}];
            CGSize adjustedSize = CGSizeMake(ceilf(textStringSize.width), ceilf(textStringSize.height));
            textAndLabelHeightDiff = labelHeight - adjustedSize.height;
            textAndLabelWidthDiff = labelWidth - adjustedSize.width;
            if (fontSizeAverage == minFontSize || fontSizeAverage == maxFontSize) {
                if (textAndLabelHeightDiff < 0 || textAndLabelWidthDiff < 0) {
                    return [self setFont:[UIFont systemFontOfSize:fontSizeAverage - 1]];
                } else {
                    return [self setFont:[UIFont systemFontOfSize:fontSizeAverage]];
                }
            }
            if (textAndLabelHeightDiff < 0 || textAndLabelWidthDiff < 0) {
                maxFontSize = fontSizeAverage - 1;
            } else if (textAndLabelHeightDiff > 0 || textAndLabelWidthDiff > 0) {
                minFontSize = fontSizeAverage + 1;
            } else {
                return [self setFont:[UIFont systemFontOfSize:fontSizeAverage]];
            }
        }
    }
    return [self setFont:[UIFont systemFontOfSize:fontSizeAverage]];
}


@end
