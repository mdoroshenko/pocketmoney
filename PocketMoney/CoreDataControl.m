//
//  CoreDataControl.m
//  PocketMoney
//
//  Created by Максим Дорошенко on 28.02.16.
//  Copyright © 2016 Max Doroshenko. All rights reserved.
//

#import "CoreDataControl.h"
#import "AppDelegate.h"
#import "GlobalVariables.h"

@implementation CoreDataControl

static NSString *const pocketEntityName = @"Pocket";
static NSString *const dailySpentsEntityName = @"DailySpents";

// Updates pocket with GlobalVariables values
- (void)updatePocket {
    DLog(@"...raised");
    Pocket *pocket = [self getPocket];

    pocket.monthlyLimit = [NSNumber numberWithInteger:[GlobalVariables sharedInstance].mLim];
    pocket.startDayLimit = [NSNumber numberWithInteger:[GlobalVariables sharedInstance].startDLim];
    pocket.curDayLimit = [NSNumber numberWithInteger:[GlobalVariables sharedInstance].curDLim];
    pocket.monthlySpent = [NSNumber numberWithInteger:[GlobalVariables sharedInstance].mSpent];
    pocket.dailySpent = [NSNumber numberWithInteger:[GlobalVariables sharedInstance].dSpent];
    pocket.curDayNumber = [NSNumber numberWithInteger:[GlobalVariables sharedInstance].curDayNumber];
    pocket.daysInMonth = [NSNumber numberWithInteger:[GlobalVariables sharedInstance].daysInMonthCount];
    pocket.absoluteDLimit = [NSNumber numberWithInteger:[GlobalVariables sharedInstance].absoluteDLim];
    pocket.curMonthNumber = [NSNumber numberWithInteger:[GlobalVariables sharedInstance].curMonthNumber];
    
    [self saveContext];
}

// Get Pocket entity from CoreData
- (Pocket *)getPocket {
    DLog(@"...raised");
    Pocket *result = nil;
    NSFetchRequest *request = [[NSFetchRequest alloc] initWithEntityName:pocketEntityName];
    NSManagedObjectContext *moc = [self getManagedObjectContext];
    NSError *error = nil;
    NSArray *objects = [moc executeFetchRequest:request error:&error];
    NSAssert(error == nil, @"Error while executing fetch request:%@\n%@", [error localizedDescription], [error description]);
    if (objects == nil || objects.count < 1) {
        result = [NSEntityDescription insertNewObjectForEntityForName:pocketEntityName inManagedObjectContext:moc];
    } else {
        result = (Pocket *)objects[0];
    }
    return result;
}

// Save CoreData
- (void)saveContext {
    DLog(@"...raised");
    [(AppDelegate *)[[UIApplication sharedApplication] delegate] saveContext];
}

- (NSManagedObjectContext *)getManagedObjectContext {
    return [(AppDelegate *)[[UIApplication sharedApplication] delegate] managedObjectContext];
}

// MARK: Daily spents
- (NSArray<DailySpents *> *)getDailySpents {
    DLog(@"...raised");
    NSArray<DailySpents *> *result = nil;
    NSFetchRequest *request = [[NSFetchRequest alloc] initWithEntityName:dailySpentsEntityName];
    NSManagedObjectContext *moc = [self getManagedObjectContext];
    NSError *error = nil;
    NSArray *objects = [moc executeFetchRequest:request error:&error];
    NSAssert(error == nil, @"Error while executing fetch request:%@\n%@", [error localizedDescription], [error description]);
    if (objects != nil && objects.count > 0) {
        result = objects;
    }
    return result;
}

- (void)clearDailySpents {
    DLog(@"...raised");
    NSFetchRequest *request = [[NSFetchRequest alloc] initWithEntityName:dailySpentsEntityName];
    NSBatchDeleteRequest *delete = [[NSBatchDeleteRequest alloc] initWithFetchRequest:request];
    NSError *error = nil;
    NSManagedObjectContext *moc = [self getManagedObjectContext];
    [moc executeRequest: delete error:&error];
    NSAssert(error == nil, @"Error while executing fetch request:%@\n%@", [error localizedDescription], [error description]);
    [self saveContext];
}

- (void)addNewDailySpent:(NSUInteger)spent forDayNumber:(NSUInteger)dayNumber {
    DLog(@"...raised");
    DailySpents *newDS = [NSEntityDescription insertNewObjectForEntityForName:dailySpentsEntityName inManagedObjectContext:[self getManagedObjectContext]];
    newDS.number = [NSNumber numberWithInteger: dayNumber];
    newDS.spent = [NSNumber numberWithInteger: spent];
    [self saveContext];
}

- (void)updateDailySpent:(NSUInteger)spent forDayNumber:(NSUInteger)dayNumber {
    DLog(@"...raised");
    NSFetchRequest *request = [[NSFetchRequest alloc] initWithEntityName:dailySpentsEntityName];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"number == %@", [NSNumber numberWithInteger:dayNumber]];
    [request setPredicate:predicate];
    NSError *error = nil;
    NSArray *objects = [[self getManagedObjectContext] executeFetchRequest:request error:&error];
    NSAssert(error == nil, @"Error while executing fetch request:%@\n%@", [error localizedDescription], [error description]);
    if (objects != nil && objects.count > 0) {
        NSAssert(objects.count == 1, @"Bad objects count!");
        DailySpents *spents = (DailySpents *)objects[0];
        spents.spent = [NSNumber numberWithInteger: spent];
        [self saveContext];
    } else {
        [self addNewDailySpent:spent forDayNumber:dayNumber];
    }
}

@end
