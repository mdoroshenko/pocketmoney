//
//  UIView+Animations.h
//  PocketMoney
//
//  Created by Максим Дорошенко on 01.03.16.
//  Copyright © 2016 Max Doroshenko. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIView (Animations) <UITextViewDelegate>

// Create animated label
- (void)createAnimatedLabelSlideFromRightAndResize:(NSString * _Nonnull)text fontSize:(CGFloat)fontSize destination:(CGPoint)destination textOffset:(CGSize)textOffset duration:(CGFloat)secs textColor:(UIColor * _Nonnull)textColor completion:(nullable void(^)(BOOL))completion;

// Create animated textView
- (void)createAnimatedTextViewSlideFromRightAndResize:(NSString * _Nonnull)text fontSize:(CGFloat)fontSize destination:(CGPoint)destination textOffset:(CGSize)textOffset duration:(CGFloat)secs textColor:(UIColor * _Nonnull)textColor completion:(nullable void(^)(BOOL))completion;

// Create animated textView with close button
- (void)createAnimatedTextViewSlideFromRightAndResizeWithCloseButton:(NSString * _Nonnull)text fontSize:(CGFloat)fontSize destination:(CGPoint)destination textOffset:(CGSize)textOffset duration:(CGFloat)secs textColor:(UIColor * _Nonnull)textColor closeButton:(UIButton * _Nonnull)closeButton completion:(nullable void(^)(BOOL))completion;

// Move to
- (void)moveTo:(CGPoint)destination duration:(float)secs option:(UIViewAnimationOptions)option completion:(nullable void(^)(BOOL))completion;
// Move to and resize
- (void)moveToAndResize:(CGPoint)destination destinationFrame:(CGRect)frame duration:(float)secs options:(UIViewAnimationOptions)options completion:(nullable void(^)(BOOL))completion;

@end
