//
//  ValueForKeyPathWithIndexes.h
//  PocketMoney
//
//  Created by Максим Дорошенко on 03.03.16.
//  Copyright © 2016 Max Doroshenko. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSObject (ValueForKeyPathWithIndexes)

-(id)valueForKeyPathWithIndexes:(NSString*)fullPath;

@end
