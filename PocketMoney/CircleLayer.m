//
//  CircleLayer.m
//  PocketMoney
//
//  Created by Максим Дорошенко on 03.03.16.
//  Copyright © 2016 Max Doroshenko. All rights reserved.
//

#import "CircleLayer.h"
#import "InterfaceConstants.h"
#import "ColorsPalette.h"
#import "GlobalVariables.h"

@implementation CircleLayer

- (instancetype)init
{
    self = [super init];
    if (self) {
        self.shadowColor = [[ColorsPalette sharedInstance].shadowColor CGColor];
        [self setNeedsDisplay];
    }
    return self;
}

- (instancetype)initWithX:(CGFloat)x y:(CGFloat)y color:(UIColor *)color {
    self = [self init];
    if (self) {
        if ([self respondsToSelector:@selector(setContentsScale:)]) {
            self.contentsScale = [[UIScreen mainScreen] scale];
        }
        self.mainColor = color;
        self.frame = CGRectMake(0, 0, pockmon_icGRAPH_CIRCLE_RADIUS * 2, pockmon_icGRAPH_CIRCLE_RADIUS * 2);
        self.position = CGPointMake(x, y);
    }
    return self;
}

- (id)initWithLayer:(id)layer {
    if(self = [super initWithLayer:layer]) {
        if ([layer isKindOfClass:[CircleLayer class]]) {
            CircleLayer *other = (CircleLayer *)layer;
            self.position = other.position;
        }
    }
    return self;
}

// If position.y != y then update
- (void)tryNewY:(CGFloat)y withColor:(UIColor *)color {
    self.mainColor = color;
    if (self.position.y != y) {
        self.position = CGPointMake(self.position.x, y);
    } else {
        [self setNeedsDisplay];
    }
}

-(CABasicAnimation *)makeAnimationForKey:(NSString *)key {
    CABasicAnimation *anim = [CABasicAnimation animationWithKeyPath:key];
    anim.fromValue = [[self presentationLayer] valueForKey:key];
    anim.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseOut];
    anim.duration = 0.5;
    
    return anim;
}

-(id<CAAction>)actionForKey:(NSString *)event {
    if ([event isEqualToString:@"position"]) {
        return [self makeAnimationForKey:event];
    }
    
    return [super actionForKey:event];
}

- (void)drawInContext:(CGContextRef)ctx {
    CGContextSetFillColorWithColor(ctx, [self.mainColor CGColor]);
    CGContextSetShadowWithColor(ctx, CGSizeMake(pockmon_icSHADOW_OFFSET_X, pockmon_icSHADOW_OFFSET_Y), pockmon_icSHADOW_BLUR, self.shadowColor);
    // Some magic for shadows, decrease rect
    CGFloat delta = pockmon_icSHADOW_OFFSET_X > pockmon_icSHADOW_OFFSET_Y ? pockmon_icSHADOW_OFFSET_X : pockmon_icSHADOW_OFFSET_Y;
    CGRect rectForEllipse = CGRectInset(self.bounds, delta * 2, delta * 2);
    CGContextFillEllipseInRect(ctx, rectForEllipse);
}

// Set needsDisplayForKey is key is @"path"
+ (BOOL)needsDisplayForKey:(NSString *)key {
    if ([key isEqualToString:@"position"]) {
        return YES;
    }
    return [super needsDisplayForKey:key];
}

@end
