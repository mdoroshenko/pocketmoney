//
//  GraphView.h
//  PocketMoney
//
//  Created by Максим Дорошенко on 27.02.16.
//  Copyright © 2016 Max Doroshenko. All rights reserved.
//
//  Base class for graphs

#import <UIKit/UIKit.h>
#import "BigView.h"
#import "ColorsPalette.h"
#import "UIView+Helpers.h"
#import "InterfaceConstants.h"
#import "GlobalVariables.h"

@interface GraphView : BigView

@property (nonatomic, strong, nonnull) UIColor *graphLineColor;
@property (nonatomic, strong, nonnull) UIColor *graphCircleColor;

@property (nonatomic, strong, nullable) UIBezierPath *curPath;

// DrawGraph for <forDays> days count, if <forDays> == 0, then draw for all days
- (void)drawGraph:(NSUInteger)forDays context:(_Nonnull CGContextRef)context bounds:(CGRect)bounds xInset:(CGFloat)xInset gridSteps:(NSUInteger)gridSteps unitsMultiplier:(CGFloat)unitsMultiplier;

@end
