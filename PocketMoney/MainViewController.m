//
//  ViewController.m
//  PocketMoney
//
//  Created by Максим Дорошенко on 25.02.16.
//  Copyright © 2016 Max Doroshenko. All rights reserved.
//

#import "MainViewController.h"
#import "SpendingControl.h"
#import "NewMonthViewController.h"
#import "NotificationConstants.h"
#import "KeyboardView.h"
#import "UIView+Animations.h"
#import "InterfaceConstants.h"
#import "ColorsPalette.h"
#import "UIView+Keyboard.h"

@interface MainViewController ()
@property (weak, nonatomic) IBOutlet UILabel *cashLabel;

@property (weak, nonatomic) IBOutlet UILabel *dailyLimitLabel;


@end

@implementation MainViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(newMonthStarts) name:NNAME_newMonthStarts object:nil];

    // TODO: Fix this
    UIFont *defFont = [UIFont fontWithName:@"Courier New" size:35];
    self.dailyLimitLabel.font = defFont;
    self.cashLabel.font = defFont;
    [self.view setBackgroundColor:[ColorsPalette sharedInstance].backgroundColor];
}

- (void)viewDidAppear:(BOOL)animated {
    // Create SpendingControl singleton
    [SpendingControl sharedInstance];
    // Add observer to dLimit changes
    self.dailyLimitLabel.text = [NSString stringWithFormat:@"%lu", [GlobalVariables sharedInstance].curDLim];
    [[GlobalVariables sharedInstance] addObserver:self forKeyPath:@"curDLim" options:NSKeyValueObservingOptionNew context:nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary<NSString *,id> *)change context:(void *)context {
    if ([keyPath isEqualToString:@"curDLim"]) {
        DLog(@"...raised");
        NSNumber *i = (NSNumber *)[change objectForKey:NSKeyValueChangeNewKey];
        NSUInteger dayLimit = [i integerValue];
        dispatch_async(dispatch_get_main_queue(), ^{
            self.dailyLimitLabel.text = [NSString stringWithFormat:@"%lu", dayLimit];
        });
    }
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    [[GlobalVariables sharedInstance] removeObserver:self forKeyPath:@"curDLim"];
}

- (IBAction)spentButtonClicked:(id)sender {
    [self.view makeKeyboardWithDelegate:[SpendingControl sharedInstance] canBeCancelled:YES];
}

- (void)newMonthStarts {
    NewMonthViewController *nextMonthViewController = [[NewMonthViewController alloc] init];
    [self presentViewController:nextMonthViewController animated:YES completion:nil];
}

@end
