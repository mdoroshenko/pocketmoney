//
//  UIView+Helpers.m
//  PocketMoney
//
//  Created by Максим Дорошенко on 01.03.16.
//  Copyright © 2016 Max Doroshenko. All rights reserved.
//

#import "UIView+Helpers.h"

@implementation UIView (Helpers)

// Draw line across the <points>
- (void)drawBezierLine:(CGContextRef)context points:(CGPoint[])points count:(NSUInteger)count linetWidth:(CGFloat)width color:(UIColor *)color {
    CGContextSetStrokeColorWithColor(context, [color CGColor]);
    CGContextSetLineWidth(context, width);
    [[self generateBezierLineFromPoints:points count:count] stroke];
}

// Generate UIBezierPath with points[]
- (UIBezierPath *)generateBezierLineFromPoints:(CGPoint[])points count:(NSUInteger)count {
    UIBezierPath *path = [UIBezierPath bezierPath];
    [path moveToPoint:points[0]];
    for (NSUInteger i = 1; i < count; i++) {
        [path addLineToPoint:points[i]];
    }
    return path;
}

// Draw circles with centers in <centers>
- (void)drawCircles:(CGContextRef)context radius:(CGFloat)rad centers:(CGPoint[])centers count:(NSUInteger)count {
    for (int i = 0; i < count; i++) {
        [self drawCircle:context radius:rad center:centers[i]];
    }
}

// Draw single circle
- (void)drawCircle:(CGContextRef)context radius:(CGFloat)rad center:(CGPoint)center {
    CGContextSetFillColorWithColor(context, [[UIColor blueColor] CGColor]);
    CGContextFillEllipseInRect(context, CGRectMake(center.x - rad, center.y - rad, rad * 2, rad * 2));
}

// Draw rounded rect
- (void)drawRaundedRect:(CGContextRef)context inRect:(CGRect)rect roundedRadius:(CGFloat)radius {
    UIBezierPath *path = [[UIBezierPath alloc] init];
    
    // Fill path, clockwise
    // top start point
    CGFloat tsX = rect.origin.x + radius;
    CGFloat tsY = rect.origin.y;
    // top end point
    CGFloat teX = rect.origin.x + rect.size.width - radius;
    
    [path moveToPoint:CGPointMake(tsX, tsY)];
    [path addLineToPoint:CGPointMake(teX, tsY)];
    
    // right start point
    CGFloat rsX = rect.origin.x + rect.size.width;
    CGFloat rsY = rect.origin.y + radius;
    [path addArcWithCenter:CGPointMake(teX, rsY) radius:radius startAngle:1.5 * M_PI endAngle:0 clockwise:1];
    
    // right end point
    CGFloat reY = rect.origin.y + rect.size.height - radius;
    [path addLineToPoint:CGPointMake(rsX, reY)];
    
    // Bottom start point (teX, tsX)
    // Bottom end point
    CGFloat bsY = rect.origin.y + rect.size.height;
    [path addArcWithCenter:CGPointMake(teX, reY) radius:radius startAngle:0 endAngle:M_PI_2 clockwise:1];
    [path addLineToPoint:CGPointMake(tsX, bsY)];
    
    // Close path
    [path addArcWithCenter:CGPointMake(tsX, reY) radius:radius startAngle:M_PI_2 endAngle:M_PI clockwise:1];
    [path addLineToPoint:CGPointMake(rect.origin.x, rsY)];
    [path addArcWithCenter:CGPointMake(tsX, rsY) radius:radius startAngle:M_PI endAngle:1.5 * M_PI clockwise:1];
    [path stroke];
    [path fill];
}


// Draw grid for days count
- (void)drawGrid:(CGContextRef)c inBounds:(CGRect)r forDays:(NSUInteger)days {
    NSUInteger inset = 20;
    CGFloat xStep = (r.size.width - 2 * inset) / (days - 1);
    NSUInteger y1 = r.size.height - inset;
    NSUInteger y2 = inset;
    
    CGFloat dash[] = {2.0, 8.0};
    CGContextSetLineDash(c, 0.0, dash, 2);
    CGContextSetLineWidth(c, 0.8);
    CGContextSetStrokeColorWithColor(c, [[UIColor lightGrayColor] CGColor]);
    
    for (int i = 0; i < days; i++) {
        CGFloat x = inset + xStep * i;
        CGContextMoveToPoint(c, x, y1);
        CGContextAddLineToPoint(c, x, y2);
    }
    CGContextStrokePath(c);
    CGContextSetLineDash(c, 0.0, nil, 0);
}

@end
