//
//  GraphLayer.m
//  PocketMoney
//
//  Created by Максим Дорошенко on 03.03.16.
//  Copyright © 2016 Max Doroshenko. All rights reserved.
//

#import "GraphLayer.h"
#import "InterfaceConstants.h"
#import "ColorsPalette.h"
#import "GlobalVariables.h"

@implementation GraphLayer

@dynamic path;

- (instancetype)init {
    self = [super init];
    if (self) {
        [self setup];
        [self setNeedsDisplay];
    }
    
    return self;
}

// MUST BE!!! Returns pre-copy for animation process
- (id)initWithLayer:(id)layer {
    if (self = [super initWithLayer:layer]) {
        if ([layer isKindOfClass:[GraphLayer class]]) {
            GraphLayer *other = (GraphLayer *)layer;
            self.path = other.path;
            self.strokeWidth = other.strokeWidth;
            self.contentsScale = other.contentsScale;
            self.mainColor = other.mainColor;
        }
    }
    return self;
}

- (void)setup {
    self.shadowColor = [[ColorsPalette sharedInstance].shadowColor CGColor];
    self.strokeWidth = 2.0;
    if ([self respondsToSelector:@selector(setContentsScale:)]) {
        self.contentsScale = [[UIScreen mainScreen] scale];
    }
}

-(CABasicAnimation *)makeAnimationForKey:(NSString *)key {
    CABasicAnimation *anim = [CABasicAnimation animationWithKeyPath:key];
    anim.fromValue = [[self presentationLayer] valueForKey:key];
    anim.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseOut];
    anim.duration = 0.5;
    
    return anim;
}

-(id<CAAction>)actionForKey:(NSString *)event {
    if ([event isEqualToString:@"path"]) {
        return [self makeAnimationForKey:event];
    }
    
    return [super actionForKey:event];
}

- (void)drawInContext:(CGContextRef)ctx {
    CGContextSetStrokeColorWithColor(ctx, [self.mainColor CGColor]);
    CGContextSetLineWidth(ctx, self.strokeWidth);    
    CGContextAddPath(ctx, self.path);
    CGContextSetShadowWithColor(ctx, CGSizeMake(pockmon_icSHADOW_OFFSET_X, pockmon_icSHADOW_OFFSET_Y), pockmon_icSHADOW_BLUR, self.shadowColor);
    CGContextStrokePath(ctx);
}

// Set needsDisplayForKey is key is @"path"
+ (BOOL)needsDisplayForKey:(NSString *)key {
    if ([key isEqualToString:@"path"]) {
        return YES;
    }
    return [super needsDisplayForKey:key];
}

@end
