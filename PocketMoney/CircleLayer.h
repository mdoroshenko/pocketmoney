//
//  CircleLayer.h
//  PocketMoney
//
//  Created by Максим Дорошенко on 03.03.16.
//  Copyright © 2016 Max Doroshenko. All rights reserved.
//

#import <QuartzCore/QuartzCore.h>
#import <UIKit/UIKit.h>

@interface CircleLayer : CALayer

@property (nonatomic, strong, nonnull) UIColor *mainColor;

- (instancetype _Nonnull)initWithX:(CGFloat)x y:(CGFloat)y color:(UIColor * _Nonnull)color;

// If position.y != y then update
- (void)tryNewY:(CGFloat)y withColor:(UIColor * _Nonnull)color;

@end
