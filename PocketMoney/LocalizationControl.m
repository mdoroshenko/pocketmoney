//
//  LocalizationControl.m
//  PocketMoney
//
//  Created by Максим Дорошенко on 06.03.16.
//  Copyright © 2016 Max Doroshenko. All rights reserved.
//

#import "LocalizationControl.h"

@implementation LocalizationControl

- (instancetype)init
{
    self = [super init];
    if (self) {
        [self rus];
    }
    return self;
}

+ (instancetype)sharedInstance {
    static id sharedInstance;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedInstance = [[LocalizationControl alloc] init];
    });
    return sharedInstance;
}

// Setup Russian strings
- (void)rus {
    self.VIEW_NEXTMONTH_Title = @"Начался новый месяц!";
    self.VIEW_NEXTMONTH_helpTitle = @"Укажите сумму, которую Вы готовы потратить в этом месяце, и приложение поможет проконтролировать бюджет на каждый день!";
    self.VIEW_NEXTMONTH_resultsMainPart = @"В прошлом месяце Вы\n";
    self.VIEW_NEXTMONTH_resulstGoodPart = @"сэкономили ";
    self.VIEW_NEXTMONTH_resultsBadPart = @"потратили больше на ";
    self.VIEW_NEXTMONTH_newMonthTitle = @"Сколько вы готовы потратить в этом месяце?";
    self.VIEW_NEXTMONTH_newMonthTitleNotFullMonthPart = @"Сколько вы готовы потратить в течение дней, оставшихся до конца месяца?";
    self.VIEW_NEXTMONTH_startButtonTitle = @"Старт!";
    self.VIEW_NEXTMONTH_helpCloseButton = @"ОК";
    self.VIEW_KEYBOARD_сloseButtonTitle = @"Отмена";
    self.VIEW_KEYBOARD_okButtonTitle = @"ОК";
    
}

@end
