//
//  BigViewWithDefaultFillColor.m
//  PocketMoney
//
//  Created by Максим Дорошенко on 06.03.16.
//  Copyright © 2016 Max Doroshenko. All rights reserved.
//

#import "BigViewWithDefaultFillColor.h"
#import "ColorsPalette.h"

@implementation BigViewWithDefaultFillColor

- (void)drawRect:(CGRect)rect {
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextSaveGState(context);
    CGContextSetFillColorWithColor(context, [[ColorsPalette sharedInstance].lightColor CGColor]);
    [super drawRect:rect];
    CGContextRestoreGState(context);
}

@end
