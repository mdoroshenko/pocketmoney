//
//  GlobalVariables.h
//  PocketMoney
//
//  Created by Максим Дорошенко on 25.02.16.
//  Copyright © 2016 Max Doroshenko. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface GlobalVariables : NSObject

// Main monthly limit
@property (assign, nonatomic) NSUInteger mLim;
// Days in month
@property (assign, nonatomic) NSUInteger daysInMonthCount;

// Initial day limit of the day, not month!
@property (assign, nonatomic) NSUInteger startDLim;
// Initial day limit of month
@property (assign, nonatomic) NSUInteger absoluteDLim;
// Current, fixed day limit
@property (assign, nonatomic) NSUInteger curDLim;

// Monthly spent
@property (assign, nonatomic) NSUInteger mSpent;
// Dayly spent
@property (assign, nonatomic) NSUInteger dSpent;

// Current day number
@property (assign, nonatomic) NSUInteger curDayNumber;

// Get current month number
@property (assign, nonatomic) NSUInteger curMonthNumber;

+ (instancetype)sharedInstance;
// Returns new daily limit
- (NSUInteger)newDLimInMonth;
// Returns new daily limit for next day
- (NSUInteger)newDLimInMonthForNextDay;

// Current biggest value, for draw units calculation
@property (nonatomic, assign) NSUInteger curBiggestValue;

- (NSUInteger)getRealDayOfWeek;

@end
