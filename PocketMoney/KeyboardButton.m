//
//  KeyboardButton.m
//  PocketMoney
//
//  Created by Максим Дорошенко on 01.03.16.
//  Copyright © 2016 Max Doroshenko. All rights reserved.
//

#import "KeyboardButton.h"
#import "InterfaceConstants.h"
#import "UIView+Helpers.h"
#import "ColorsPalette.h"

@implementation KeyboardButton

- (instancetype)initWithCoder:(NSCoder *)coder
{
    self = [super initWithCoder:coder];
    if (self) {
        [self setup];
    }
    return self;
}

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self setup];
    }
    return self;
}

- (void)setup {
    self.keyColor = [ColorsPalette sharedInstance].keyColor;
    self.pressedKeyColor = [ColorsPalette sharedInstance].pressedKeyColor;
    self.keyTitleColor = [ColorsPalette sharedInstance].keyTitleColor;
    self.currentColor = self.keyColor;
    self.shadowColor = [ColorsPalette sharedInstance].shadowColor;
    [self setTitleColor:self.keyTitleColor forState:UIControlStateNormal];
    [self addTarget:self action:@selector(buttonTouchDown) forControlEvents:UIControlEventTouchDown];
    [self addTarget:self action:@selector(buttonTouchUp) forControlEvents:UIControlEventTouchCancel | UIControlEventTouchUpInside | UIControlEventTouchUpOutside];
}

- (void)buttonTouchDown {
    self.currentColor = self.pressedKeyColor;
    dispatch_async(dispatch_get_main_queue(), ^{[self setNeedsDisplay];});
}

- (void)buttonTouchUp {
    self.currentColor = self.keyColor;
    dispatch_async(dispatch_get_main_queue(), ^{[self setNeedsDisplay];});
}

- (void)drawRect:(CGRect)rect {
    [super drawRect:rect];
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextSaveGState(context);
    CGContextSetFillColorWithColor(context, [self.currentColor CGColor]);
    CGContextSetShadowWithColor(context, CGSizeMake(pockmon_icSHADOW_OFFSET_X, pockmon_icSHADOW_OFFSET_Y), pockmon_icSHADOW_BLUR, [self.shadowColor CGColor]);
    CGRect insetRect = CGRectInset(rect, pockmon_icBIGVIEW_INSET, pockmon_icBIGVIEW_INSET);
    [self drawRaundedRect:context inRect:insetRect roundedRadius:pockmon_icCORNER_RADIUS];
    CGContextRestoreGState(context);
}

@end
