//
//  KeyboardDelegate.h
//  PocketMoney
//
//  Created by Максим Дорошенко on 06.03.16.
//  Copyright © 2016 Max Doroshenko. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol KeyboardDelegate <NSObject>

@optional
- (void)inputEnds:(NSUInteger)inputValue;
- (void)inputViewDismissed;

@end
