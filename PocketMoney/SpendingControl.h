//
//  SpendingControl.h
//  PocketMoney
//
//  Created by Максим Дорошенко on 25.02.16.
//  Copyright © 2016 Max Doroshenko. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Pocket.h"
#import "GlobalVariables.h"
#import "KeyboardDelegate.h"

@interface SpendingControl : NSObject <KeyboardDelegate>

+ (id)sharedInstance;

// Coins spent
- (void)spentCoins:(NSUInteger)num;

@end
