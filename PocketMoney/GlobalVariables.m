//
//  GlobalVariables.m
//  PocketMoney
//
//  Created by Максим Дорошенко on 25.02.16.
//  Copyright © 2016 Max Doroshenko. All rights reserved.
//
//  Logic:
//  NewDayStarts


#import "GlobalVariables.h"
#import "CoreDataControl.h"
#import "NSCalendar+PocketMoneyUtils.h"

@implementation GlobalVariables

+ (instancetype)sharedInstance {
    static dispatch_once_t onceToken;
    static id sharedInstance;
    dispatch_once(&onceToken, ^{
        sharedInstance = [[GlobalVariables alloc] init];
    });
    return sharedInstance;
}

- (instancetype)init
{
    self = [super init];
    if (self) {
        //TODO: only for test
//        [self updateMonthyLimit:30000 daysInMonthCount:30 startWeeklyLimit:7000 currentWeeklyLimit:7000 startDailyLimit:1000 currentDailyLimit:1000 daysInMonthRemain:30 daysInWeekRemain:7 spentInMonth:0 spentInWeek:0 spentInDay:0 curDayNumber:1];
        Pocket *pocket = [[[CoreDataControl alloc] init] getPocket];
        _mLim = [[pocket monthlyLimit] integerValue];
        _startDLim = [[pocket startDayLimit] integerValue];
        _curDLim = [[pocket curDayLimit] integerValue];
        _mSpent = [[pocket monthlySpent] integerValue];
        _dSpent = [[pocket dailySpent] integerValue];
        _daysInMonthCount = [[pocket daysInMonth] integerValue];
        _curDayNumber = [[pocket curDayNumber] integerValue];
        _absoluteDLim = [[pocket absoluteDLimit] integerValue];
        _curMonthNumber = [[pocket curMonthNumber] integerValue];
        [self checkForBiggestValue];
        [self noZeroes];
    }
    return self;
}

// Check and repair bad/zero values, if no CoreData currently exist
- (void)noZeroes {
    if (_mLim == 0) {
        _mLim = 1;
    }
    if (_daysInMonthCount == 0) {
        _daysInMonthCount = [[NSCalendar currentCalendar] daysInCurrentMonth];
    }
    if (_absoluteDLim == 0) {
        _absoluteDLim = 1;
    }
    if (_curBiggestValue == 0) {
        _curBiggestValue = 1;
    }
}

- (NSUInteger)getRealDayOfWeekOffset {
    NSUInteger result = [[NSCalendar currentCalendar] getCurrentDayOfWeek];
    if (result == 1) {
        result = 8;
    }
    result = result - _curDayNumber % 7;
    return result;
}

- (NSUInteger)getRealDayOfWeek {
    NSUInteger result = [[NSCalendar currentCalendar] getCurrentDayOfWeek];
    if (result == 1) {
        result = 8;
    }
    result--;
    return result;
}

// Setter for mLim (New month starts)
- (void)setMLim:(NSUInteger)mLim {
    _mSpent = 0;
    _dSpent = 0;
    _curDayNumber = [self getCurDayNumber];
    _daysInMonthCount = [[NSCalendar currentCalendar] daysInCurrentMonth];
    
    _mLim = mLim;
    _startDLim = [self newDLimInMonth];
    _curDLim = _startDLim;
    _absoluteDLim = _startDLim;
    _curMonthNumber = [[NSCalendar currentCalendar] getCurrentMonth];
    [[[CoreDataControl alloc] init] clearDailySpents];
    [self fillPrevDaysWithZero];
    self.dSpent = 0;
    
    self.curBiggestValue = _startDLim;
}

- (NSUInteger)getCurDayNumber {
    return [[NSCalendar currentCalendar] getCurrentDayNumber];
}

// Check for biggestValue
- (void)checkForBiggestValue {
    NSUInteger result = self.curBiggestValue;
    
    NSArray<DailySpents *>* dailySpents = [[[CoreDataControl alloc] init] getDailySpents];
    for (int i = 0; i < dailySpents.count; i++) {
        if ([dailySpents[i].spent integerValue] > result) {
            result = [dailySpents[i].spent integerValue];
        }
    }
    
    if (_startDLim > result) {
        result = _startDLim;
    }
    
    if (result > self.curBiggestValue) {
        DLog(@"curBiggestValue updates, old value: %lu, new value: %lu", self.curBiggestValue, result);
        self.curBiggestValue = result;
    }
}

// Create all previous days with zero zpents then month starts
- (void)fillPrevDaysWithZero {
    if (self.curDayNumber > 1) {
        CoreDataControl *cdCtrl = [[CoreDataControl alloc] init];
        for (int i = 1; i < self.curDayNumber; i++) {
            [cdCtrl addNewDailySpent:0 forDayNumber:i];
        }
    }
}


// Setter for dSpent
- (void)setDSpent:(NSUInteger)dSpent {
    if (dSpent > self.curBiggestValue) {
        self.curBiggestValue = dSpent;
    }
    _dSpent = dSpent;
    if (self.startDLim > self.dSpent) {
        self.curDLim = self.startDLim - self.dSpent;
    } else {
        self.curDLim = 0;
    }
    CoreDataControl *cdCtrl = [[CoreDataControl alloc] init];
    [cdCtrl updatePocket];
    [cdCtrl updateDailySpent:_dSpent forDayNumber:_curDayNumber];
}

// Setter for curDayNumber
// curDSpent -> wSpent and mSpent
- (void)setCurDayNumber:(NSUInteger)curDayNumber {
    NSUInteger prevDayNumber = _curDayNumber;
    _curDayNumber = curDayNumber;
    _startDLim = [self newDLimInMonth];
    for (NSUInteger i = prevDayNumber + 1; i < _curDayNumber; i++) {
        [[[CoreDataControl alloc] init] addNewDailySpent:0 forDayNumber:i];
    }
    self.dSpent = 0;
}

// Returns new daily limit
- (NSUInteger)newDLimInMonth {
    if (_mLim < _mSpent) {
        return 0;
    } else {
        return (_mLim - _mSpent) / (_daysInMonthCount - _curDayNumber + 1);
    }
}

// Returns new daily limit for next day
- (NSUInteger)newDLimInMonthForNextDay {
    if (_mLim < _mSpent) {
        return 0;
    } else {
        if (_daysInMonthCount > _curDayNumber) {
            return (_mLim - _mSpent) / (_daysInMonthCount - _curDayNumber);
        } else { // if _daysInMonthCount == _curDayNumber, then return all current cash
            return (_mLim - _mSpent);
        }
    }
}

@end
