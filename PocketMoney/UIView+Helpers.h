//
//  UIView+Helpers.h
//  PocketMoney
//
//  Created by Максим Дорошенко on 01.03.16.
//  Copyright © 2016 Max Doroshenko. All rights reserved.
//
// Some draw helpers

#import <UIKit/UIKit.h>

@interface UIView (Helpers)

// Draw line across the <points>
- (void)drawBezierLine:(CGContextRef)context points:(CGPoint[])points count:(NSUInteger)count linetWidth:(CGFloat)width color:(UIColor *)color;

// Draw circles with centers in <centers>
- (void)drawCircles:(CGContextRef)context radius:(CGFloat)rad centers:(CGPoint[])centers count:(NSUInteger)count;

// Draw single circle
- (void)drawCircle:(CGContextRef)context radius:(CGFloat)rad center:(CGPoint)center;

// Draw rounded rect
- (void)drawRaundedRect:(CGContextRef)context inRect:(CGRect)rect roundedRadius:(CGFloat)radius;

// Draw grid for days count
- (void)drawGrid:(CGContextRef)c inBounds:(CGRect)r forDays:(NSUInteger)days;

// Generate UIBezierPath with points[]
- (UIBezierPath *)generateBezierLineFromPoints:(CGPoint[])points count:(NSUInteger)count;

@end
