//
//  NSCalendar+PocketMoneyUtils.h
//  PocketMoney
//
//  Created by Максим Дорошенко on 28.02.16.
//  Copyright © 2016 Max Doroshenko. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSCalendar (PocketMoneyUtils)

// Returns days count in current month
- (NSUInteger)daysInCurrentMonth;
// Gets current day number
- (NSUInteger)getCurrentDayNumber;

// Gets current day of a week
- (NSUInteger)getCurrentDayOfWeek;

// Gets current month
- (NSUInteger)getCurrentMonth;

@end
