//
//  NotificationConstants.h
//  PocketMoney
//
//  Created by Максим Дорошенко on 26.02.16.
//  Copyright © 2016 Max Doroshenko. All rights reserved.
//

#ifndef NotificationConstants_h
#define NotificationConstants_h

static NSString *NNAME_newMonthStarts = @"com.max.PocketMoney.NEW_MONTH_STARTS";
static NSString *NNAME_userActivatesNewMonthWithLimit = @"com.max.PocketMoney.USER_ACTIVATES_NEW_MONTH_WITH_LIMIT";
static NSString *NKEY_newLimit = @"newLimit";

#endif /* NotificationConstants_h */
