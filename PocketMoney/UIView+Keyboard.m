//
//  UIView+Keyboard.m
//  PocketMoney
//
//  Created by Максим Дорошенко on 05.03.16.
//  Copyright © 2016 Max Doroshenko. All rights reserved.
//

#import "UIView+Keyboard.h"
#import "InterfaceConstants.h"

@implementation UIView (Keyboard)

// Adds animated keyboard to current view
- (void)makeKeyboardWithDelegate:(id<KeyboardDelegate>)delegate canBeCancelled:(BOOL)canBeCancelled {
    CGRect frameRect = CGRectMake(self.bounds.origin.x,
                                  self.bounds.origin.y + self.bounds.size.height,
                                  self.bounds.size.width,
                                  pockmon_icSPENTVIEW_HEIGHT);
    CGRect insetRect = CGRectInset(frameRect, 20 - pockmon_icBIGVIEW_INSET * 2.0f, 5);
    CGFloat endY = self.bounds.origin.y + self.bounds.size.height - pockmon_icSPENTVIEW_HEIGHT - pockmon_icSPENTVIEW_OFFSET_Y;
    KeyboardView *keybView = [[KeyboardView alloc] initWithFrame:insetRect];
    keybView.canBeCancelled = canBeCancelled;
    keybView.delegate = delegate;
    [keybView presentInView:self endAnimationPoint:CGPointMake(insetRect.origin.x, endY)];
    
}

@end
