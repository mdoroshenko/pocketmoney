//
//  CoreDataControl.h
//  PocketMoney
//
//  Created by Максим Дорошенко on 28.02.16.
//  Copyright © 2016 Max Doroshenko. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Pocket.h"
#import "DailySpents.h"

@interface CoreDataControl : NSObject

// Updates pocket with GlobalVariables values
- (void)updatePocket;
// Get Pocket entity from CoreData
- (Pocket *)getPocket;

// MARK: Daily spents
- (NSArray<DailySpents *> *)getDailySpents;
- (void)clearDailySpents;
- (void)addNewDailySpent:(NSUInteger)spent forDayNumber:(NSUInteger)dayNumber;
- (void)updateDailySpent:(NSUInteger)spent forDayNumber:(NSUInteger)dayNumber;

@end
