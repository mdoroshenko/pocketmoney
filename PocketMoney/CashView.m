//
//  CashView.m
//  PocketMoney
//
//  Created by Максим Дорошенко on 01.03.16.
//  Copyright © 2016 Max Doroshenko. All rights reserved.
//

#import "CashView.h"
#import "ColorsPalette.h"

@implementation CashView

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIColor clearColor];
    }
    return self;
}

- (instancetype)initWithCoder:(NSCoder *)coder
{
    self = [super initWithCoder:coder];
    if (self) {
        self.backgroundColor = [UIColor clearColor];
    }
    return self;
}

- (void)drawRect:(CGRect)rect {
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextSaveGState(context);
    CGContextSetFillColorWithColor(context, [[ColorsPalette sharedInstance].lightColor CGColor]);
    [super drawRect:rect];
    CGContextRestoreGState(context);
}

@end
