//
//  LocalizationControl.h
//  PocketMoney
//
//  Created by Максим Дорошенко on 06.03.16.
//  Copyright © 2016 Max Doroshenko. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface LocalizationControl : NSObject

@property (nonatomic, strong, nonnull) NSString *VIEW_NEXTMONTH_Title;
@property (nonatomic, strong, nonnull) NSString *VIEW_NEXTMONTH_helpTitle;
@property (nonatomic, strong, nonnull) NSString *VIEW_NEXTMONTH_resultsMainPart;
@property (nonatomic, strong, nonnull) NSString *VIEW_NEXTMONTH_resultsBadPart;
@property (nonatomic, strong, nonnull) NSString *VIEW_NEXTMONTH_resulstGoodPart;
@property (nonatomic, strong, nonnull) NSString *VIEW_NEXTMONTH_newMonthTitle;
@property (nonatomic, strong, nonnull) NSString *VIEW_NEXTMONTH_newMonthTitleNotFullMonthPart;
@property (nonatomic, strong, nonnull) NSString *VIEW_NEXTMONTH_startButtonTitle;
@property (nonatomic, strong, nonnull) NSString *VIEW_NEXTMONTH_helpCloseButton;
@property (nonatomic, strong, nonnull) NSString *VIEW_KEYBOARD_сloseButtonTitle;
@property (nonatomic, strong, nonnull) NSString *VIEW_KEYBOARD_okButtonTitle;

+ (instancetype _Nonnull)sharedInstance;

@end
