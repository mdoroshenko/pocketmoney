//
//  NewMonthViewControllerDelegate.h
//  PocketMoney
//
//  Created by Максим Дорошенко on 26.02.16.
//  Copyright © 2016 Max Doroshenko. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol NewMonthViewControllerDelegate <NSObject>

- (void)viewControllerDismissed:(NSUInteger)newLimit;

@end
