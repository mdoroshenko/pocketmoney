//
//  BigView.h
//  PocketMoney
//
//  Created by Максим Дорошенко on 01.03.16.
//  Copyright © 2016 Max Doroshenko. All rights reserved.
//
//  Class for windows and graphs views

#import <UIKit/UIKit.h>

@interface BigView : UIView

@end
