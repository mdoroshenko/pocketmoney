//
//  ColorsPalette.m
//  PocketMoney
//
//  Created by Максим Дорошенко on 04.03.16.
//  Copyright © 2016 Max Doroshenko. All rights reserved.
//

#import "ColorsPalette.h"

@implementation ColorsPalette

- (instancetype)init
{
    self = [super init];
    if (self) {
        [self createColors];
    }
    return self;
}

- (void)createColors {
    CGFloat f = 255.0f;
    self.backgroundColor = [UIColor colorWithRed:220/f green:255/f blue:227/f alpha:1.0f];
    self.backgroundColorWithAlpha = [UIColor colorWithRed:220/f green:255/f blue:227/f alpha:0.7f];
    self.keyColor = [UIColor colorWithRed:228/f green:255/f blue:207/f alpha:1.0];
    self.pressedKeyColor = [UIColor colorWithRed:193/f green:232/f blue:189/f alpha:1.0];
    self.keyTitleColor = [UIColor blackColor];
    
    self.lightColor = [UIColor colorWithRed:207/f green:255/f blue:244/f alpha:1.0];
    self.darkColor = [UIColor colorWithRed:189/f green:232/f blue:210/f alpha:1.0];
    
    self.graphCircleColor = [UIColor colorWithRed:56/f green:232/f blue:36/f alpha:1.0];
    self.graphLineColor = [UIColor colorWithRed:52/f green:255/f blue:92/f alpha:1.0];
    
    self.lowGraphCircleColor = [UIColor colorWithRed:232/f green:100/f blue:119/f alpha:1.0];
    self.lowGraphLineColor = [UIColor colorWithRed:255/f green:136/f blue:123/f alpha:1.0];
    
    self.shadowColor = [UIColor darkGrayColor];
    
    self.titlesColor = [UIColor blackColor];
}

+ (instancetype)sharedInstance {
    static id sharedInstance;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedInstance = [[ColorsPalette alloc] init];
    });
    return sharedInstance;
}

@end
