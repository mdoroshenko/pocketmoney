//
//  MonthlyGraphView.m
//  PocketMoney
//
//  Created by Максим Дорошенко on 01.03.16.
//  Copyright © 2016 Max Doroshenko. All rights reserved.
//

#import "MonthlyGraphView.h"

@implementation MonthlyGraphView

- (void)drawRect:(CGRect)rect {
    [self updateColors];
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextSaveGState(context);
    CGContextSetFillColorWithColor(context, [[ColorsPalette sharedInstance].lightColor CGColor]);
    [super drawRect:rect];
    CGContextRestoreGState(context);
    CGRect boundsForGraph = CGRectInset(self.bounds, 20 - pockmon_icBIGVIEW_INSET, 20 - pockmon_icBIGVIEW_INSET);
    CGContextSetLineWidth(context, 2.0);
    NSUInteger days = [GlobalVariables sharedInstance].daysInMonthCount;
    [self drawGrid:context inBounds:self.bounds forDays:days];
    CGContextSetShadowWithColor(context, CGSizeMake(pockmon_icSHADOW_OFFSET_X, pockmon_icSHADOW_OFFSET_Y), pockmon_icSHADOW_BLUR, [[ColorsPalette sharedInstance].shadowColor CGColor]);
    [self drawGraph:days context:context bounds:boundsForGraph xInset:10 - pockmon_icBIGVIEW_INSET / 2 gridSteps:days unitsMultiplier:0.8];
}
- (void)updateColors {
    if ([GlobalVariables sharedInstance].absoluteDLim > [GlobalVariables sharedInstance].newDLimInMonthForNextDay) {
        if (self.graphCircleColor != [ColorsPalette sharedInstance].lowGraphCircleColor) {
            self.graphCircleColor = [ColorsPalette sharedInstance].lowGraphCircleColor;
        }
        if (self.graphLineColor != [ColorsPalette sharedInstance].lowGraphLineColor) {
            self.graphLineColor = [ColorsPalette sharedInstance].lowGraphLineColor;
        }
    } else {
        if (self.graphCircleColor != [ColorsPalette sharedInstance].graphCircleColor) {
            self.graphCircleColor = [ColorsPalette sharedInstance].graphCircleColor;
        }
        if (self.graphLineColor != [ColorsPalette sharedInstance].graphLineColor) {
            self.graphLineColor = [ColorsPalette sharedInstance].graphLineColor;
        }
    }
}

@end
