//
//  NSCalendar+PocketMoneyUtils.m
//  PocketMoney
//
//  Created by Максим Дорошенко on 28.02.16.
//  Copyright © 2016 Max Doroshenko. All rights reserved.
//

#import "NSCalendar+PocketMoneyUtils.h"

@implementation NSCalendar (PocketMoneyUtils)

- (NSUInteger)daysInCurrentMonth {
    NSDate *date = [NSDate date];
    NSRange days = [self rangeOfUnit:NSCalendarUnitDay inUnit:NSCalendarUnitMonth forDate:date];
    return days.length;
}

// Gets current month
- (NSUInteger)getCurrentMonth {
    NSCalendar *c = [NSCalendar currentCalendar];
    NSDateComponents *comp = [c components:NSCalendarUnitMonth fromDate:[NSDate date]];
    NSUInteger curMonth = [comp month];
    DLog(@"Current month: %lu", curMonth);
    return curMonth;
}

// Gets current day number
- (NSUInteger)getCurrentDayNumber {
    NSCalendar *c = [NSCalendar currentCalendar];
    NSDateComponents *comp = [c components:NSCalendarUnitDay fromDate:[NSDate date]];
    return [comp day];
}

- (NSUInteger)getCurrentDayOfWeek {
    NSCalendar *gregorian = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
    NSDateComponents *comps = [gregorian components:NSCalendarUnitWeekday fromDate:[NSDate date]];
    NSUInteger weekday = [comps weekday];
    DLog(@"The week day number: %lu", weekday);
    return weekday;
}

@end
