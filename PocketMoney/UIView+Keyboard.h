//
//  UIView+Keyboard.h
//  PocketMoney
//
//  Created by Максим Дорошенко on 05.03.16.
//  Copyright © 2016 Max Doroshenko. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "KeyboardView.h"

@interface UIView (Keyboard)

// Adds animated keyboard to current view
- (void)makeKeyboardWithDelegate:(id<KeyboardDelegate>)delegate canBeCancelled:(BOOL)canBeCancelled;

@end
