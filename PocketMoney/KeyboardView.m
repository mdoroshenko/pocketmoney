//
//  SpentView.m
//  PocketMoney
//
//  Created by Максим Дорошенко on 26.02.16.
//  Copyright © 2016 Max Doroshenko. All rights reserved.
//

#import "KeyboardView.h"
#import "SpendingControl.h"
#import "KeyboardButton.h"
#import "UIView+Animations.h"
#import "InterfaceConstants.h"
#import "ColorsPalette.h"
#import "LocalizationControl.h"

@implementation KeyboardView

static CGFloat okCancelButtonHeight = 50.0;
static CGFloat labelHeight = 50.0;
static NSUInteger maxSymbols = 6; // Exclusive
static BOOL isVisible = NO;
static CGRect boundsInset;

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIColor clearColor];
        boundsInset = CGRectInset(self.bounds, pockmon_icBIGVIEW_INSET * 2.0f, pockmon_icBIGVIEW_INSET * 2.0f);
//        [self addElements];
    }
    return self;
}

// Animated present this view in <parentView> view if isVisible = NO
- (void)presentInView:(UIView *)parentView endAnimationPoint:(CGPoint)point {
    if (!isVisible) {
        isVisible = YES;
        [self addElements];
        [parentView addSubview:self];
        [self moveTo:point duration:pockmon_icANIMATION_DURATION option:UIViewAnimationOptionBeginFromCurrentState completion:nil];
    }
}

- (void)drawRect:(CGRect)rect {
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextSaveGState(context);
    UIColor *bckColor = [ColorsPalette sharedInstance].backgroundColorWithAlpha;
    CGContextSetFillColorWithColor(context, [bckColor CGColor]);
    [super drawRect:rect];    
    CGContextRestoreGState(context);
}

- (void)addElements {
    // OK button
    CGFloat controlButtonsCount = 1.0f;
    CGFloat offsetX = 0.0f;
    if ([self canBeCancelled]) {
        controlButtonsCount = 2.0f;
        offsetX = boundsInset.size.width * 0.5f;
    }
    KeyboardButton *okButton = [KeyboardButton buttonWithType:UIButtonTypeRoundedRect];
    CGRect okButtonFrame = CGRectMake(boundsInset.origin.x + offsetX,
                                      boundsInset.size.height - okCancelButtonHeight,
                                      boundsInset.size.width / controlButtonsCount,
                                      okCancelButtonHeight);
    okButton.frame = okButtonFrame;
    [okButton setTitle:[LocalizationControl sharedInstance].VIEW_KEYBOARD_okButtonTitle forState:UIControlStateNormal];
    [okButton addTarget:self action:@selector(okButtonClicked) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:okButton];
    
    // Cancel button
    if ([self canBeCancelled]) {
        KeyboardButton *cancelButton = [KeyboardButton buttonWithType:UIButtonTypeCustom];
        CGRect cancelButtonFrame = CGRectMake(boundsInset.origin.x,
                                              boundsInset.size.height - okCancelButtonHeight,
                                              boundsInset.size.width / 2,
                                              okCancelButtonHeight);
        cancelButton.frame = cancelButtonFrame;
        [cancelButton setTitle:[LocalizationControl sharedInstance].VIEW_KEYBOARD_сloseButtonTitle forState:UIControlStateNormal];
        [cancelButton addTarget:self action:@selector(cancelButtonClicked) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:cancelButton];
    }
    
    CGFloat offset = 5.0;
    CGRect keybRect = CGRectMake(boundsInset.origin.x,
                                 boundsInset.origin.y + labelHeight + offset,
                                 boundsInset.size.width,
                                 boundsInset.size.height - okCancelButtonHeight - labelHeight - offset * 2);
    [self addKeyboardInRect:keybRect];
    
    CGRect labelRect = CGRectMake(boundsInset.origin.x, boundsInset.origin.y, boundsInset.size.width, labelHeight);
    CGRect offsetLabelRect = CGRectOffset(labelRect, 40, 0);
    [self makePrintLabelWithFrame:offsetLabelRect];
}

// Make print label
- (void)makePrintLabelWithFrame:(CGRect)rect {
    self.label = [[UILabel alloc] initWithFrame:rect];
    self.label.text = @"0";
    [self addSubview:self.label];
}

// Make keyboard
- (void)addKeyboardInRect:(CGRect)rect {
    // Size of keyboard
    short numH = 4;
    short numV = 3;
    
    float keyWidth = rect.size.width / numH;
    float keyHeight = rect.size.height / numV;
    for (short i = 0; i < numH; i++) {
        for (short n = 0; n < numV; n++) {
            float x = rect.origin.x + keyWidth * i;
            float y = rect.origin.y + keyHeight * n;
            short buttonNum = n * numH + i;
            NSString *title;
            if (buttonNum < 10) {
                title = [NSString stringWithFormat:@"%d", buttonNum];
            } else if (buttonNum == 10) {
                title = @"<-";
            }
            if (buttonNum < 11) {
                KeyboardButton *newKeybButton = [KeyboardButton buttonWithType:UIButtonTypeCustom];
                if (buttonNum < 10) {
                    newKeybButton.frame = CGRectMake(x, y, keyWidth, keyHeight);
                } else {
                    newKeybButton.frame = CGRectMake(x, y, keyWidth * 2, keyHeight); // Backspace button double width
                }
                
                [newKeybButton setTitle:title forState:UIControlStateNormal];
                [newKeybButton addTarget:self action:@selector(keyboardButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
                [self addSubview:newKeybButton];
            }
        }
    }
    // Last line of a keyboard
}

- (void)keyboardButtonClicked: (id)sender {
    if ([sender isKindOfClass:[UIButton class]]) {
        UIButton *b = (UIButton *)sender;
        DLog(@"Button <%@> pressed", b.titleLabel.text);
        if ([b.titleLabel.text isEqualToString:@"<-"]) {
            if (self.label.text.length > 1) {
                NSMutableString *newText = [NSMutableString stringWithString:self.label.text];
                self.label.text = [newText substringToIndex:newText.length - 1];
            } else {
                [self.label setText:@"0"];
            }
        } else {
            if (self.label.text.length == 1 && [self.label.text isEqualToString:@"0"]) {
                self.label.text = b.titleLabel.text;
            } else if (self.label.text.length < maxSymbols) {
                self.label.text = [self.label.text stringByAppendingString:b.titleLabel.text];
            }
        }
    }
}


- (void)cancelButtonClicked {
    [self removeFromSuperview];
}

- (void)okButtonClicked {
    NSUInteger spent = [self.label.text integerValue];
    if (spent != 0) {
        [self.delegate inputEnds:spent];
        [self removeFromSuperview];
    }
}

- (void)removeFromSuperview {
    isVisible = NO;
    CGPoint endPoint = CGPointMake(self.frame.origin.x, self.frame.origin.y + pockmon_icSPENTVIEW_HEIGHT + pockmon_icSPENTVIEW_OFFSET_Y);
    [self moveTo:endPoint duration:pockmon_icANIMATION_DURATION option:UIViewAnimationOptionBeginFromCurrentState completion:^(BOOL isEnds) {
        if ([self.delegate respondsToSelector:@selector(inputViewDismissed)]) {
            [self.delegate inputViewDismissed];
        }
        [super removeFromSuperview];
    }];
}

@end
