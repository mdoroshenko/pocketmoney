//
//  SpentLabel.m
//  PocketMoney
//
//  Created by Максим Дорошенко on 01.03.16.
//  Copyright © 2016 Max Doroshenko. All rights reserved.
//

#import "SpentLabel.h"
#import "UIView+Helpers.h"
#import "InterfaceConstants.h"

@implementation SpentLabel

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self updateProperties];
    }
    return self;
}

- (void)updateProperties {
//    [self setBackgroundColor:[UIColor clearColor]];
    self.textAlignment = NSTextAlignmentCenter;
    [self setTextColor:[UIColor blackColor]];
//    [self setTintAdjustmentMode:UIViewTintAdjustmentModeAutomatic];
//    [self setAdjustsFontSizeToFitWidth:YES];
//    [self setNumberOfLines:1];
//    [self setBaselineAdjustment:UIBaselineAdjustmentAlignBaselines];
//    [self setMinimumScaleFactor:10.0/12.0];
    [self setClipsToBounds:NO];
    
}

- (void)drawRect:(CGRect)rect {
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGRect offsetRect = CGRectOffset(rect, 10, 10);
//    CGContextSaveGState(context);
//    CGContextSetFillColorWithColor(context, [[UIColor whiteColor] CGColor]);
    [self drawRaundedRect:context inRect:offsetRect roundedRadius:pockmon_icCORNER_RADIUS];
//    CGContextRestoreGState(context);
//    CGContextSetFillColorWithColor(context, [[UIColor clearColor] CGColor]);
//    [self.text drawAtPoint:CGPointMake(self.bounds.origin.x + self.bounds.size.width / 2.0, self.bounds.origin.y + self.bounds.size.height / 2.0) withAttributes:nil];
}

@end
