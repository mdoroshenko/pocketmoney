//
//  WeeklyGraphView.m
//  PocketMoney
//
//  Created by Максим Дорошенко on 27.02.16.
//  Copyright © 2016 Max Doroshenko. All rights reserved.
//

#import "WeeklyGraphView.h"

@implementation WeeklyGraphView

- (void)drawRect:(CGRect)rect {
    [self updateColors];
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextSaveGState(context);
    CGContextSetFillColorWithColor(context, [[ColorsPalette sharedInstance].lightColor CGColor]);
    [super drawRect:rect];
    CGContextRestoreGState(context);
    CGRect boundsForGraph = CGRectInset(self.bounds, 20 - pockmon_icBIGVIEW_INSET, 20 - pockmon_icBIGVIEW_INSET);
    CGContextSetLineWidth(context, 2.0);
    [self drawGrid:context inBounds:self.bounds forDays:7];
    CGContextSetShadowWithColor(context, CGSizeMake(pockmon_icSHADOW_OFFSET_X, pockmon_icSHADOW_OFFSET_Y), pockmon_icSHADOW_BLUR, [[ColorsPalette sharedInstance].shadowColor CGColor]);
    NSUInteger drawDays = [[GlobalVariables sharedInstance] getRealDayOfWeek] ;
    [self drawGraph:drawDays context:context bounds:boundsForGraph xInset:10 - pockmon_icBIGVIEW_INSET / 2 gridSteps:7 unitsMultiplier:0.8];
}

- (void)updateColors {
    if ([GlobalVariables sharedInstance].curDLim == 0) {
        if (self.graphCircleColor != [ColorsPalette sharedInstance].lowGraphCircleColor) {
            self.graphCircleColor = [ColorsPalette sharedInstance].lowGraphCircleColor;
        }
        if (self.graphLineColor != [ColorsPalette sharedInstance].lowGraphLineColor) {
            self.graphLineColor = [ColorsPalette sharedInstance].lowGraphLineColor;
        }
    } else {
        if (self.graphCircleColor != [ColorsPalette sharedInstance].graphCircleColor) {
            self.graphCircleColor = [ColorsPalette sharedInstance].graphCircleColor;
        }
        if (self.graphLineColor != [ColorsPalette sharedInstance].graphLineColor) {
            self.graphLineColor = [ColorsPalette sharedInstance].graphLineColor;
        }
    }
}


@end
