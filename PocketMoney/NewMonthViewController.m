//
//  NewMonthViewController.m
//  PocketMoney
//
//  Created by Максим Дорошенко on 26.02.16.
//  Copyright © 2016 Max Doroshenko. All rights reserved.
//

#import "NewMonthViewController.h"
#import "NotificationConstants.h"
#import "ColorsPalette.h"
#import "BigViewWithDefaultFillColor.h"
#import "UIView+Animations.h"
#import "LocalizationControl.h"
#import "InterfaceConstants.h"
#import "KeyboardButton.h"
#import "NSCalendar+PocketMoneyUtils.h"


@interface NewMonthViewController ()

@property (nonatomic, strong, nullable) UIView *helpView;

@end

@implementation NewMonthViewController

- (void)loadView {
    self.view = [[UIView alloc] initWithFrame:[UIScreen mainScreen].bounds];
    [self.view setBackgroundColor:[ColorsPalette sharedInstance].backgroundColor];
}

- (void)viewDidLoad {
    [super viewDidLoad];
}

- (void)viewDidAppear:(BOOL)animated {
    [self showViews];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)showViews {
    

    
    // titleView
    BigViewWithDefaultFillColor *titleView = [[BigViewWithDefaultFillColor alloc]
                                              initWithFrame:CGRectMake(self.view.bounds.origin.x + self.view.bounds.size.width,
                                                                       self.view.bounds.origin.y + 20.0f,
                                                                       self.view.bounds.size.width - 20.0f,
                                                                       10.0f)];
    // helpView as completion block of titleView
    void(^titleCompletion)(BOOL) = ^(BOOL b){
        BigViewWithDefaultFillColor *helpView = [[BigViewWithDefaultFillColor alloc]
                                                 initWithFrame:CGRectMake(self.view.bounds.origin.x + self.view.bounds.size.width,
                                                                          self.view.bounds.origin.y + 20.0f + titleView.frame.size.height,
                                                                          self.view.bounds.size.width - 20.0f,
                                                                          10.0f)];
        [helpView setBackgroundColor:[UIColor clearColor]];
        [self.view addSubview: helpView];
        CGPoint moveToPointHelp = CGPointMake(self.view.bounds.origin.x + 10.0f, self.view.bounds.origin.y + 20.0f + titleView.frame.size.height);
        
        NSString *helpViewText = [LocalizationControl sharedInstance].VIEW_NEXTMONTH_helpTitle;
        KeyboardButton *closeHelpButton = [[KeyboardButton alloc] initWithFrame:CGRectMake(0, 0, helpView.bounds.size.width, 40.0f)];
        [closeHelpButton setTitle:[LocalizationControl sharedInstance].VIEW_NEXTMONTH_helpCloseButton forState:UIControlStateNormal];
        [closeHelpButton addTarget:self action:@selector(closeHelpView) forControlEvents:UIControlEventTouchUpInside];
        [helpView createAnimatedTextViewSlideFromRightAndResizeWithCloseButton:helpViewText
                                                                      fontSize:pockmon_icFONT_SIZE_STANDART
                                                                   destination:moveToPointHelp
                                                                    textOffset:CGSizeMake(pockmon_icANIMATED_TITLE_OFFSET_X, pockmon_icANIMATED_TITLE_OFFSET_Y)
                                                                      duration:0.5f
                                                                     textColor:[ColorsPalette sharedInstance].titlesColor
                                                                   closeButton:closeHelpButton
                                                                    completion:nil];
        self.helpView = helpView;
    };
    // helpView ends
    
    [titleView setBackgroundColor:[UIColor clearColor]];
    [self.view addSubview: titleView];
    CGPoint moveToPoint = CGPointMake(self.view.bounds.origin.x + 10.0f, self.view.bounds.origin.y + 20.0f);

    NSString *titleString = [LocalizationControl sharedInstance].VIEW_NEXTMONTH_Title;
    [titleView createAnimatedLabelSlideFromRightAndResize:titleString
                                                 fontSize:pockmon_icFONT_SIZE_STANDART
                                              destination:moveToPoint
                                               textOffset:CGSizeMake(pockmon_icANIMATED_TITLE_OFFSET_X, pockmon_icANIMATED_TITLE_OFFSET_Y)
                                                 duration:0.5f
                                                textColor:[ColorsPalette sharedInstance].titlesColor
                                               completion:titleCompletion];
    
    
    
}

// Close helpView button pressed
- (void)closeHelpView {
    if (self.helpView != nil) {
        NSArray<UIView *> *subviews = self.helpView.subviews;
        for (int i = 0; i < subviews.count; i++) {
            dispatch_async(dispatch_get_main_queue(), ^{
                [subviews[i] removeFromSuperview];
            });
        }
        // Calculate new frame for message and keyboard
        NSString *message = [LocalizationControl sharedInstance].VIEW_NEXTMONTH_newMonthTitle;
        if ([[NSCalendar currentCalendar] getCurrentDayNumber] > 1) {
            message = [LocalizationControl sharedInstance].VIEW_NEXTMONTH_newMonthTitleNotFullMonthPart;
        }
        CGFloat desiredWidth = self.helpView.bounds.size.width - 2.0f * pockmon_icANIMATED_TITLE_OFFSET_X;
        UITextView *testView = [[UITextView alloc] init];
        [testView setFont:[UIFont systemFontOfSize:pockmon_icFONT_SIZE_STANDART]];
        [testView setText:message];
        [testView setBackgroundColor:[UIColor clearColor]];
        [testView setDelegate:self];
        [testView setTextAlignment:NSTextAlignmentCenter];
        CGSize fitSize = [testView sizeThatFits:CGSizeMake(desiredWidth,
                                                           CGFLOAT_MAX)];
        [testView sizeToFit];
        CGFloat desiredHeight = fitSize.height;
        
        
        
        [UIView animateWithDuration:0.5f animations:^{
            self.helpView.frame = CGRectMake(self.helpView.frame.origin.x,
                                             self.helpView.frame.origin.y,
                                             self.helpView.frame.size.width,
                                             desiredHeight + pockmon_icANIMATED_TITLE_OFFSET_Y);
        } completion:^(BOOL b){
            [testView setFrame:CGRectMake(self.helpView.bounds.origin.x + pockmon_icANIMATED_TITLE_OFFSET_X,
                                          self.helpView.bounds.origin.y + pockmon_icANIMATED_TITLE_OFFSET_Y * 0.5f,
                                          self.helpView.bounds.size.width - pockmon_icANIMATED_TITLE_OFFSET_X * 2.0f,
                                          desiredHeight)];
            [self.helpView addSubview:testView];
            [self.view makeKeyboardWithDelegate:self canBeCancelled:NO];
        }];
    }
}

// MARK: UITextViewDelegate
- (BOOL)textViewShouldBeginEditing:(UITextView *)textView {
    return NO;
}

// MARK: KeyboardDelegate
- (void)inputEnds:(NSUInteger)inputValue {
    [self dismissViewControllerAnimated:YES completion:^{
        NSNumber *newLimit = [NSNumber numberWithInteger:inputValue];
        NSDictionary *userInfo = @{NKEY_newLimit: newLimit};
        [[NSNotificationCenter defaultCenter] postNotificationName:NNAME_userActivatesNewMonthWithLimit object:self userInfo:userInfo];
    }];
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
