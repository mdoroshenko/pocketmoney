//
//  Point.m
//  PocketMoney
//
//  Created by Максим Дорошенко on 03.03.16.
//  Copyright © 2016 Max Doroshenko. All rights reserved.
//

#import "GraphPoint.h"

@implementation GraphPoint

- (instancetype)initWithX:(float)x y:(float)y
{
    self = [self init];
    if (self) {
        self.x = x;
        self.y = y;
    }
    return self;
}

@end
