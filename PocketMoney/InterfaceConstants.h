//
//  InterfaceConstants.h
//  PocketMoney
//
//  Created by Максим Дорошенко on 01.03.16.
//  Copyright © 2016 Max Doroshenko. All rights reserved.
//

#ifndef InterfaceConstants_h
#define InterfaceConstants_h

static CGFloat pockmon_icCORNER_RADIUS = 1.0;
static CGFloat pockmon_icSHADOW_BLUR = 1.0;
static CGFloat pockmon_icSHADOW_OFFSET_X = 1.0;
static CGFloat pockmon_icSHADOW_OFFSET_Y = 1.0;

static CGFloat pockmon_icBIGVIEW_INSET = 2.0;

static CGFloat pockmon_icSPENTVIEW_OFFSET_Y = 10.0;
static CGFloat pockmon_icSPENTVIEW_HEIGHT = 300.0;

static CGFloat pockmon_icANIMATION_DURATION = 0.3;

static CGFloat pockmon_icGRAPH_CIRCLE_RADIUS = 6.0;

static CGFloat pockmon_icFONT_SIZE_MAX = 50.0f;
static CGFloat pockmon_icFONT_SIZE_MIN = 6.0f;
static CGFloat pockmon_icFONT_SIZE_STANDART = 17.0f;

static CGFloat pockmon_icANIMATED_TITLE_OFFSET_X = 10.0f;
static CGFloat pockmon_icANIMATED_TITLE_OFFSET_Y = 20.0f;

#endif /* InterfaceConstants_h */
