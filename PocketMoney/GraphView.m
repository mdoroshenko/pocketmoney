//
//  GraphView.m
//  PocketMoney
//
//  Created by Максим Дорошенко on 27.02.16.
//  Copyright © 2016 Max Doroshenko. All rights reserved.
//

#import "GraphView.h"
#import "DailySpents.h"
#import "CoreDataControl.h"
#import "GlobalVariables.h"
#import "UIView+Helpers.h"
#import "GraphLayer.h"
#import "GraphPoint.h"
#import "CircleLayer.h"
#import "InterfaceConstants.h"
#import "WeeklyGraphView.h"

@implementation GraphView

- (instancetype)initWithCoder:(NSCoder *)coder
{
    self = [super initWithCoder:coder];
    if (self) {
        self.backgroundColor = [UIColor clearColor];
        [[GlobalVariables sharedInstance] addObserver:self forKeyPath:@"dSpent" options:NSKeyValueObservingOptionNew context:nil];
    }
    return self;
}

- (void)dealloc
{
    [[GlobalVariables sharedInstance] removeObserver:self forKeyPath:@"dSpent"];
}

-(void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary<NSString *,id> *)change context:(void *)context {
    if ([keyPath isEqualToString:@"dSpent"]) {
        dispatch_async(dispatch_get_main_queue(), ^{[self setNeedsDisplay];});
    }
}

- (NSArray<DailySpents *>*)getDSpents {
    return [[[CoreDataControl alloc] init] getDailySpents];
}

- (void)sortDSpents:(NSArray<DailySpents *>*)spents {
    NSMutableArray<DailySpents *> *sort = [[NSMutableArray alloc] initWithArray:spents copyItems:NO];
    for (NSUInteger i = 0; i < sort.count; i++) {
        NSUInteger chkI = i;
        while (chkI > 0 && [sort[i].number integerValue] > [sort[i-1].number integerValue] ) {
            DailySpents *tmp = sort[i-1];
            sort[i-1] = sort[i];
            sort[i] = tmp;
        }
    }
    spents = [sort copy];
}

// DrawGraph for last <forDays> days count, if <forDays> == 0, then draw for all days
- (void)drawGraph:(NSUInteger)forDays context:(CGContextRef)context bounds:(CGRect)bounds xInset:(CGFloat)xInset gridSteps:(NSUInteger)gridSteps unitsMultiplier:(CGFloat)unitsMultiplier {
    
    if (forDays == 0) {
        forDays = [GlobalVariables sharedInstance].daysInMonthCount;
    }
    
    NSArray<DailySpents *> *dailySpents = [self getDSpents];
    [self sortDSpents:dailySpents];
    
    // TODO: Baaad, baaaaad, baaad! Parent class knows about subclass :(
    if (![self isKindOfClass:[WeeklyGraphView class]] && dailySpents.count < forDays) {
        forDays = dailySpents.count;
    }
    
    NSMutableArray <NSNumber *> *dSpents = [[NSMutableArray alloc] init];
    
    NSUInteger dayOffset = 0;
    
    // TODO: Baaad, baaaaad, baaad! Parent class knows about subclass :(
    if ([self isKindOfClass:[WeeklyGraphView class]] &&  dailySpents.count < forDays) {
        for (int i = 0; i < forDays - dailySpents.count; i++) {
            [dSpents addObject:[NSNumber numberWithInt:0]];
        }
    }
    if ([self isKindOfClass:[WeeklyGraphView class]]) {
        if (dailySpents.count >= [[GlobalVariables sharedInstance] getRealDayOfWeek]) {
            dayOffset = dailySpents.count - [[GlobalVariables sharedInstance] getRealDayOfWeek];
        } else {
            for (int i = 0; i < [[GlobalVariables sharedInstance] getRealDayOfWeek] - dailySpents.count - 1; i++) {
                [dSpents addObject:[NSNumber numberWithInt:0]];
            }
        }
        
    }
    for (NSUInteger i = dayOffset; i < dailySpents.count; i++) {
        [dSpents addObject:dailySpents[i].spent];
    }

    NSInteger startDLimit = [GlobalVariables sharedInstance].absoluteDLim;
    CGFloat units;
    if ([GlobalVariables sharedInstance].curBiggestValue == 0) {
        units = 10.0f;
    } else {
       units = (bounds.size.height * 0.5) / [GlobalVariables sharedInstance].curBiggestValue;
    }
    units *= unitsMultiplier;
    CGFloat xStep = bounds.size.width / (gridSteps - 1);
    CGFloat yCenter = bounds.origin.y + bounds.size.height * 0.5;
    // No days for draw
    if (forDays == 0) {
        return;
    }
    
    // Create array of points
    CGPoint points[forDays];
    for (int i = 0; i < forDays; i++) {
        CGFloat x = bounds.origin.x + xStep * i;
        CGFloat y = yCenter + (startDLimit - [dSpents[i] integerValue]) * units;
        NSAssert(!isnan(x) && !isnan(y), @"Bad coords!");
        points[i] = CGPointMake(x, y);
    }
    
    // Draw stuff here
    if (self.curPath == nil) {
        self.curPath = [self generateBezierLineFromPoints:points count:forDays];
        GraphLayer *gLayer = [[GraphLayer alloc] init];
        gLayer.mainColor = self.graphLineColor;
        gLayer.frame = self.bounds;
        [self.layer addSublayer:gLayer];
        
        gLayer.path = [self.curPath CGPath];
        
        for (int i = 0; i < forDays; i++) {
            [self.layer addSublayer:[[CircleLayer alloc] initWithX:points[i].x y:points[i].y color:self.graphCircleColor]];
        }
    } else {
        UIBezierPath *newPath = [self generateBezierLineFromPoints:points count:forDays];
        GraphLayer *gLayer = (GraphLayer *)self.layer.sublayers[0];
        gLayer.mainColor = self.graphLineColor;
        gLayer.path = [newPath CGPath];
        self.curPath = newPath;
        
        // Same count
        if (self.layer.sublayers.count - 1 == forDays) {
            // Update
            for (int i = 1; i < forDays + 1; i++) {
                [(CircleLayer *)self.layer.sublayers[i] tryNewY:points[i-1].y withColor:self.graphCircleColor];
            }
            // More then
        } else if (forDays > self.layer.sublayers.count - 1) {
            // Update
            for (int i = 1; i < self.layer.sublayers.count; i++) {
                [(CircleLayer *)self.layer.sublayers[i] tryNewY:points[i-1].y withColor:self.graphCircleColor];
            }
            // And add
            for (NSUInteger i = self.layer.sublayers.count - 1; i < forDays; i++) {
                [self.layer addSublayer:[[CircleLayer alloc] initWithX:points[i].x y:points[i].y color:self.graphCircleColor]];
            }
        } else {
            NSMutableArray<CALayer *> *layersToRemove = [[NSMutableArray alloc] init];
            for (NSUInteger i = forDays + 1; i < self.layer.sublayers.count; i++) {
                [layersToRemove addObject:self.layer.sublayers[i]];
            }
            for (NSUInteger i = 0; i < layersToRemove.count; i++) {
                [layersToRemove[i] removeFromSuperlayer];
            }
            // Update
            for (int i = 1; i < self.layer.sublayers.count; i++) {
                [(CircleLayer *)self.layer.sublayers[i] tryNewY:points[i-1].y withColor:self.graphCircleColor];
            }
        }
    }
    [self drawStartDLimitLine:context xFrom:bounds.origin.x - xInset xTo:bounds.origin.x + bounds.size.width + xInset y:yCenter];
}

// Draw startDLimit horizontal line
- (void)drawStartDLimitLine:(CGContextRef)context xFrom:(CGFloat)xFrom xTo:(CGFloat)xTo y:(CGFloat)y {
    CGContextMoveToPoint(context, xFrom, y);
    CGContextSetStrokeColorWithColor(context, [[UIColor grayColor] CGColor]);
    CGContextSetLineWidth(context, 1.0);
    CGContextAddLineToPoint(context, xTo, y);
    CGContextStrokePath(context);
}

@end
