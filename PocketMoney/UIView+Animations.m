//
//  UIView+Animations.m
//  PocketMoney
//
//  Created by Максим Дорошенко on 01.03.16.
//  Copyright © 2016 Max Doroshenko. All rights reserved.
//

#import "UIView+Animations.h"

@implementation UIView (Animations)

// Create animated label
- (void)createAnimatedLabelSlideFromRightAndResize:(NSString *)text fontSize:(CGFloat)fontSize destination:(CGPoint)destination textOffset:(CGSize)textOffset duration:(CGFloat)secs textColor:(UIColor *)textColor completion:(nullable void(^)(BOOL))completion {
    
    CGSize titleSize = [text sizeWithAttributes:@{NSFontAttributeName:[UIFont systemFontOfSize:fontSize]}];
    
    CGRect frameForTitleView = CGRectMake(destination.x,
                                          destination.y,
                                          self.bounds.size.width,
                                          titleSize.height + textOffset.height * 2);
    CGRect labelFrame = CGRectMake(textOffset.width,
                                   textOffset.height,
                                   frameForTitleView.size.width - textOffset.width * 2,
                                   titleSize.height);
    [self moveToAndResize:destination
              destinationFrame:frameForTitleView
                      duration:0.5f
                       options:UIViewAnimationOptionCurveEaseOut
                    completion:^(BOOL b){
                        UILabel *titleLabel = [[UILabel alloc] initWithFrame:labelFrame];
                        [titleLabel setText:text];
                        [self addSubview:titleLabel];
                        [titleLabel setAdjustsFontSizeToFitWidth:YES];
                        [titleLabel setTextAlignment:NSTextAlignmentCenter];
                        [titleLabel setTextColor:textColor];
                        [titleLabel setFont:[UIFont systemFontOfSize:fontSize]];
                        if (completion != nil) {
                            completion(b);
                        }
                    }];

}

// Create animated label
- (void)createAnimatedTextViewSlideFromRightAndResize:(NSString *)text fontSize:(CGFloat)fontSize destination:(CGPoint)destination textOffset:(CGSize)textOffset duration:(CGFloat)secs textColor:(UIColor *)textColor completion:(nullable void(^)(BOOL))completion {
    
    CGFloat desiredWidth = self.bounds.size.width - textOffset.width * 2.0f;
    UITextView *testView = [[UITextView alloc] init];
    [testView setFont:[UIFont systemFontOfSize:fontSize]];
    [testView setText:text];
    CGSize desiredSize = [testView sizeThatFits:CGSizeMake(desiredWidth, CGFLOAT_MAX)];
    CGFloat desiredHeight = desiredSize.height;
    
    CGRect frameForTitleView = CGRectMake(destination.x,
                                          destination.y,
                                          self.bounds.size.width,
                                          desiredHeight + textOffset.height * 2);
    CGRect labelFrame = CGRectMake(textOffset.width,
                                   textOffset.height,
                                   frameForTitleView.size.width - 2.0f * textOffset.width,
                                   desiredHeight);
    [self moveToAndResize:destination
         destinationFrame:frameForTitleView
                 duration:0.5f
                  options:UIViewAnimationOptionCurveEaseOut
               completion:^(BOOL b){
                   UITextView *titleView = [[UITextView alloc] initWithFrame:labelFrame];
                   [titleView setText:text];
                   [self addSubview:titleView];
                   [titleView setTextAlignment:NSTextAlignmentCenter];
                   [titleView setTextColor:textColor];
                   [titleView setBackgroundColor:[UIColor clearColor]];
                   [titleView setDelegate:self];
                   [titleView setFont:[UIFont systemFontOfSize:fontSize]];
                   if (completion != nil) {
                       completion(b);
                   }
               }];
    
}

// Create animated textView with close button
- (void)createAnimatedTextViewSlideFromRightAndResizeWithCloseButton:(NSString *)text
                                                            fontSize:(CGFloat)fontSize
                                                         destination:(CGPoint)destination
                                                          textOffset:(CGSize)textOffset
                                                            duration:(CGFloat)secs
                                                           textColor:(UIColor *)textColor
                                                         closeButton:(UIButton *)closeButton
                                                          completion:(nullable void(^)(BOOL))completion {
    
    void(^updateCompletionWithButton)(BOOL) = ^(BOOL b) {
        closeButton.frame = CGRectMake(closeButton.frame.origin.x + textOffset.width,
                                       self.bounds.size.height - closeButton.frame.origin.y - closeButton.frame.size.height - textOffset.height,
                                       closeButton.frame.size.width - textOffset.width * 2,
                                       closeButton.frame.size.height);
        [self addSubview:closeButton];
        if (completion != nil) {
            completion(b);
        }
    };
    
    void(^comletionResize)(BOOL) = ^(BOOL b){
        CGRect newFrame = CGRectMake(self.frame.origin.x, self.frame.origin.y, self.frame.size.width, self.frame.size.height + closeButton.frame.size.height + textOffset.height);
        [UIView animateWithDuration:secs/2.0f animations:^{
            self.frame = newFrame;
        } completion:updateCompletionWithButton];
    };
    [self createAnimatedTextViewSlideFromRightAndResize:text
                                               fontSize:fontSize
                                            destination:destination
                                             textOffset:textOffset
                                               duration:secs/2.0f
                                              textColor:textColor
                                             completion:comletionResize];
}

- (void)moveTo:(CGPoint)destination duration:(float)secs option:(UIViewAnimationOptions)option completion:(nullable void(^)(BOOL))completion
{
    [UIView animateWithDuration:secs delay:0.0 options:option
                     animations:^{
                         self.frame = CGRectMake(destination.x,destination.y, self.frame.size.width, self.frame.size.height);
                     }
                     completion:completion];
}

- (void)moveToAndResize:(CGPoint)destination destinationFrame:(CGRect)frame duration:(float)secs options:(UIViewAnimationOptions)options completion:(nullable void(^)(BOOL))completion {
    
    // Update completion block for unscribe from @"frame"
    void(^newCompBlock)(BOOL) = ^(BOOL b){
        [self removeObserver:self forKeyPath:@"frame"];
        if (completion != nil) {
            completion(b);
        }
    };
    
    // Register as observer for @"frame"
    [self addObserver:self forKeyPath:@"frame" options:NSKeyValueObservingOptionNew context:nil];
    
    [self moveTo:destination duration:secs/2.0f option:options completion:^(BOOL b){
        [UIView animateWithDuration:secs animations:^{
            self.frame = frame;
        } completion:newCompBlock];
    }];
}

// Observe
- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary<NSString *,id> *)change context:(void *)context {
    
    // For update shadows e.t.c
    if ([keyPath isEqualToString:@"frame"]) {
        [self setNeedsDisplay];
    }
}

// MARK: UITextViewDelegate
- (BOOL)textViewShouldBeginEditing:(UITextView *)textView {
    return NO;
}

- (bool)gestureRecognizerShouldBegin:(UIGestureRecognizer *)gestureRecognizer {
    if ([gestureRecognizer isKindOfClass:[UIPanGestureRecognizer class]]) {
        return NO;
    }
    return YES;
}

@end
