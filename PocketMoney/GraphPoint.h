//
//  Point.h
//  PocketMoney
//
//  Created by Максим Дорошенко on 03.03.16.
//  Copyright © 2016 Max Doroshenko. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface GraphPoint : NSObject

@property (nonatomic, assign) float x;
@property (nonatomic, assign) float y;

- (instancetype)initWithX:(float)x y:(float)y;

@end
