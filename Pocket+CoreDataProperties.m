//
//  Pocket+CoreDataProperties.m
//  PocketMoney
//
//  Created by Максим Дорошенко on 09.03.16.
//  Copyright © 2016 Max Doroshenko. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "Pocket+CoreDataProperties.h"

@implementation Pocket (CoreDataProperties)

@dynamic absoluteDLimit;
@dynamic curDayLimit;
@dynamic curDayNumber;
@dynamic dailySpent;
@dynamic daysInMonth;
@dynamic monthlyLimit;
@dynamic monthlySpent;
@dynamic startDayLimit;
@dynamic curMonthNumber;

@end
