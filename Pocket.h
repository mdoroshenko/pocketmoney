//
//  Pocket.h
//  PocketMoney
//
//  Created by Максим Дорошенко on 25.02.16.
//  Copyright © 2016 Max Doroshenko. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

NS_ASSUME_NONNULL_BEGIN

@interface Pocket : NSManagedObject

@end

NS_ASSUME_NONNULL_END

#import "Pocket+CoreDataProperties.h"
